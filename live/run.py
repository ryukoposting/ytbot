import requests
import sys
import os
import json
import flask
import time, threading
from flask import render_template

import google.oauth2.credentials
import google_auth_oauthlib.flow
import googleapiclient.discovery

sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))
import ytbot

ytbot.SHOW_DEBUG = True
#ytbot.VERBOSE_DEBUG = True

app = flask.Flask(__name__)
app.secret_key = 'REPLACEME'


SONG_DATA = None
LAST_TRACK = None

def updateperiodic():
    global SONG_DATA
    global LAST_TRACK
    
    result = json.loads(requests.get('https://hackerchan.org/api/current', verify=False).text)

    try:
        ytflag = result["Current"].split('?v=')[1]
        if LAST_TRACK != ytflag:
            data = ytbot.search.getsong(ytflag)
            #print(ytbot.util.prettifydict('{}'.format(data)))
            LAST_TRACK = ytflag
            SONG_DATA = data
    except:
            print('ERROR: exception while scraping')
            SONG_DATA = None
            LAST_TRACK = None
    
    threading.Timer(20, updateperiodic).start()


@app.route('/')
def mainpage():
    global SONG_DATA
    
    if SONG_DATA is not None:
        return render_template('test.html', data = SONG_DATA)
    else:
        return "Nothing currently playing!"
    


if __name__ == '__main__':
    os.environ['OAUTHLIB_INSECURE_TRANSPORT'] = '1'
    
    ytbot.auth.configure('secrets.json')
    updateperiodic()
    
    with open('server.json') as file:
        server = json.loads(file.read())
        app.run(server['ip'], server['sock'], debug=True)
