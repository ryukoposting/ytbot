import requests
import sys
import os
import flask

import google.oauth2.credentials
import google_auth_oauthlib.flow
import googleapiclient.discovery

sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))
import ytbot

ytbot.SHOW_DEBUG = True
ytbot.VERBOSE_DEBUG = True

app = flask.Flask(__name__)
app.secret_key = 'REPLACEME'


if __name__ == '__main__':
    
    y = ytbot.lastfm.Song(artist="Unknown", name="Unknown")
    
    assert 'name' in y._d
    assert 'artist' in y._d
    assert 'album' in y._d
    assert 'description' in y._d
    assert 'url' in y._d
    assert 'duration' in y._d
    
    assert 'name' in y.get('artist')._d
    assert 'url' in y.get('artist')._d
    assert 'picurl' in y.get('artist')._d
    assert 'description' in y.get('artist')._d
    assert 'similar' in y.get('artist')._d
    
    assert 'name' in y.get('album')._d
    assert 'url' in y.get('album')._d
    assert 'picurl' in y.get('album')._d
    assert 'description' in y.get('album')._d
    assert 'tracklist' in y.get('album')._d
    assert 'artist' in y.get('album')._d
    
    assert 'name' in y.get('album').get('artist')._d
    assert 'url' in y.get('album').get('artist')._d
    assert 'picurl' in y.get('album').get('artist')._d
    assert 'description' in y.get('album').get('artist')._d
    assert 'similar' in y.get('album').get('artist')._d
    
    assert ytbot.util._rniq('hello, world!', 'o', 'eee') == 'helleee, weeerld!'
    assert ytbot.util._rniq('hello, "world!"', 'o', 'eee') == 'helleee, "world!"'
    assert ytbot.util._rniq('hello, "escaped\\" quote"', 'o', 'eee') == 'helleee, "escaped\\" quote"'
    assert ytbot.util._rniq("hello, \"escaped\\\" quote\"", 'o', 'eee') == 'helleee, "escaped\\" quote"'
    assert ytbot.util._rniq("hello, \"escaped\\\" quote\"", 'o', 'o"') == 'hello", "escaped\\" quote"'
    

