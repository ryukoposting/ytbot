## ytbot: Flask Extension for YouTube Music Metadata Retrieval

ytbot integrates the Last.FM and YouTube Data APIs together into flask,
allowing web services that retrieve/play music from YouTube to provide
rich metadata, as well as sophisticated song-link-fetching tools.t

ytbot has the following dependencies, all installable through pip:

 - google-api-python-client
 - google-auth
 - google-auth-oauthlib
 - google-auth-httplib2
 - requests
 - flask

ytbot is written with to be compatible with Python 3.5.3 and newer.
The functional portion of ytbot doesn't really need Flask, but it was
designed for use with Flask, and the sample application provided in the
live/ folder uses the Flask.

## Using ytbot

*1:* you will need to set up
your own Google API account and get a Google OAuth2 Client ID and Secret.
To get started, follow the instructions [here](https://developers.google.com/youtube/v3/quickstart/python)
under "Turn on the YouTube Data API" for "Installed App." This
will give you the necessary client_secret.json file to get up and
running with the YouTube side of ytbot.

*2:* get a Last.FM API key and client secret. Put these, and the path
to your google client_secret.json into a JSON file like so:
```
{
    "lastfm-apikey": "your last.fm API key",
    "lastfm-secret": "your last.fm client secret",
    "google-authfile": "path to the client_secret.json"
}

```
These instructions will refer to this file as secrets.json.

*3.* To start up ytbot, import it and call `ytbot.auth.configure('secrets.json')`
ytbot will authenticate with Google, and store the returned session key in
secrets.json. Next time you start it, it will just use this session key again.

*4.* Check out the sample Flask deployment under the live/ folder. The sample
is currently live at: [http://dailyprog.org:9876/](http://dailyprog.org:9876/)
and it retrieved data from [tswf](https://gitlab.com/dailyprog/tswf)

## Put in a YouTube video ID, get Last.FM data out

It's pretty simple:

```
import ytbot

ytbot.auth.configure('secrets.json')

song = ytbot.search.getsong('cPCLFtxpadE')
```

Congratulations, that's it. `song` contains a Song object, defined in lastfm.py.
It has exactly two public methods: update, and get. `update` will force ytbot to
make all the API calls necessary to pull all data for this track, artist, and
album (if you don't call update, these calls are only made as needed by calls to
get()). `get` retrieves properties of the song. From a song's `get` function,
you can retrieve artist and album objects that follow this same 2-method structure.
For example, to get the song's title, just run song.get('name').

**Valid Song.get() parameters:**
    name: str = the song title. contains 'Unknown' if the song is unknown.
    duration: Duration = the song's duration, per metadata. does not
        necessarily match the length of the streaming service's artifact.
    artist: Artist = the song's artist. Refer to the Artist class for
        properties of an artist.
    album: Album = the song's album. Refer to the Album class for
        properties of an album.
    description: str = description of song, if available. If not
        avalable, contains 'No description available for this song.'
    url: str = last.fm URL for the song. if a YouTube URL was provided,
        and the last.fm request fails, this will contain the youtube URL.
        otherwise, will contain empty string.
    yturl: str = the YouTube link associated with this Song.

**Valid Album.get() parameters:**
    name: str = the album title. contains 'Unknown' if the album is unknown
    artist: Artist = the album artist represented as an Album object. If you
        provide this field at initialization, you may give a string with the
        artist's name instead of an Artist object.
    picurl: str = URL for album artwork.
    tracklist: [Song] = list of Song objects representing the album's track list.
    description: str = description of album. Contains 'No description available 
        for this album.' if no description is available.
    url: str = URL for the album's last.fm profile. empty string if album is
        unknown.

**Valid Artist.get() parameters:**
    name: str = the artist's name. contains 'Unknown' if artist is unknown.
    picurl: str = URL for artist photo. contains a URL to a generic
        placeholder image if the artist is unknown.
    description: str = artist's full description. contains 'No description 
        available for this artist.' if the artist has no description or
        the artist is unknown.
    ontour: bool = true if the artist is on tour. false if the artist is
        unknown.
    similar: [Artist] = a list of artists related to this artist. empty list
        if artist is unknown.
    url: str = URL for the artist's last.fm profile. empty string if artist
        is unknown.

