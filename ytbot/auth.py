# contain authorization routines for all services.
import requests
import sys
import os
import json

import google.oauth2.credentials
from googleapiclient.discovery import build
from googleapiclient.errors import HttpError
from google_auth_oauthlib.flow import InstalledAppFlow

def configure(credentials: str, asfile: bool = True):
    credfile = credentials
    
    SCOPES = ['https://www.googleapis.com/auth/youtube.force-ssl']
    API_SERVICE_NAME = 'youtube'
    API_VERSION = 'v3'
    
    if asfile:
        with open(credfile) as file:
            credentials = json.loads(file.read())
    else:
        credfile = 'ytbot_secrets.json'
    
    if not all([k in credentials for k in ['lastfm-apikey', 
                                           'lastfm-secret',
                                           'google-authfile']]):
        raise ValueError('Missing required fields from ytbot configuration.')

    google_service = None
    if not all([k in credentials for k in ['google-id-token', 
                                           'google-refresh-token',
                                           'google-client-id',
                                           'google-client-secret',
                                           'google-scopes',
                                           'google-token-uri']]):
        flow = InstalledAppFlow.from_client_secrets_file(
            CLIENT_SECRETS_FILE, SCOPES)
        creds = flow.run_console()
        google_service = build(API_SERVICE_NAME, API_VERSION,
            credentials = creds)
        
        credentials['google-id-token'] = creds.id_token
        credentials['google-refresh-token'] = creds.refresh_token
        credentials['google-token-uri'] = creds.token_uri
        credentials['google-client-id'] = creds.client_id
        credentials['google-client-secret'] = creds.client_secret
        credentials['google-scopes'] = creds.scopes
        with open(credfile, 'w') as file:
                json.dump(credentials, file)
        
    else:
        creds = google.oauth2.credentials.Credentials(token=None,
            refresh_token = credentials['google-refresh-token'],
            id_token      = credentials['google-id-token'],
            token_uri     = credentials['google-token-uri'],
            client_id     = credentials['google-client-id'],
            client_secret = credentials['google-client-secret'])
        google_service = build(API_SERVICE_NAME, API_VERSION,
            credentials = creds)

    GoogleAuth.session(google_service)
    LastFMAuth.apikey(credentials['lastfm-apikey'])


# more of a container of state than a class, but whatever
class GoogleAuth:
    _session = None
    
    # TODO: wrap in mutex
    @staticmethod
    def session(s = None):
        if s is not None:
            GoogleAuth._session = s
        return GoogleAuth._session


class LastFMAuth:
    _apikey = None
    
    # TODO: wrap in mutex
    @staticmethod
    def apikey(s = None):
        if s is not None:
            LastFMAuth._apikey = s
        return LastFMAuth._apikey
