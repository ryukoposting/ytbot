
import google.oauth2.credentials
import google_auth_oauthlib.flow
import googleapiclient.discovery

import ytbot


def searchvideos(service, **kwargs):
    ytbot.util.debugmsg('youtube API call to videos.search')
    return service.search().list(
        **kwargs
    ).execute()


def listchannel(service, **kwargs):
    ytbot.util.debugmsg('youtube API call to channel.list')
    return service.channels().list(
      **kwargs
    ).execute()


def listvideo(service, **kwargs):
    ytbot.util.debugmsg('youtube API call to videos.list')
    return service.videos().list(
      **kwargs
    ).execute()
