__version__ = '0.1.0'


"""
behavior:
SHOW_DEBUG  VERBOSE_DEBUG       Behavior
  False        False            Don't print debug information.
  False        True             Don't print debug information.
  True         False            Print brief debug information.
  True         True             Print extra-wordy debug information.
"""
SHOW_DEBUG = False
VERBOSE_DEBUG = False

from ytbot import auth
from ytbot import lastfm
from ytbot import youtube
from ytbot import cache
from ytbot import search
from ytbot import util
