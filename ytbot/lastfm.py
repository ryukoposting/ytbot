# unsafe wrappers for Last.FM API calls
import requests
import json
import threading

import ytbot


LASTFM_URL = "http://ws.audioscrobbler.com/2.0"

def trackinfo(name=None, artist=None, mbid=None):
    ytbot.util.debugmsg('last.fm API call to track.getInfo(name={}, artist={}, mbid={})'
                            .format(name, artist, mbid))
    r = requests.post(LASTFM_URL, 
        params = { 'api_key': ytbot.auth.LastFMAuth.apikey(),
                   'method': 'track.getInfo',
                   'artist': artist,
                   'track': name,
                   'mbid': mbid,
                   'format': 'json' })
    try:
        return json.loads(r.text)['track']
    except:
        return None


def artistinfo(name=None, mbid=None):
    ytbot.util.debugmsg('last.fm API call to artist.getInfo(name={}, mbid={})'
                            .format(name, mbid))
    r = requests.post(LASTFM_URL, 
            params = { 'api_key': ytbot.auth.LastFMAuth.apikey(),
                       'method': 'artist.getInfo',
                       'artist': name,
                       'mbid': mbid,
                       'format': 'json' })
    try:
        return json.loads(r.text)['artist']
    except:
        return None


def albuminfo(name=None, artist=None, mbid=None):
    ytbot.util.debugmsg('last.fm API call to album.getInfo(name={}, artist={}, mbid={})'
                            .format(name, artist, mbid))
    r = requests.post(LASTFM_URL, 
                params = { 'api_key': ytbot.auth.LastFMAuth.apikey(),
                           'method': 'album.getInfo',
                           'artist': artist,
                           'album': name,
                           'mbid': mbid,
                           'format': 'json' })
    try:
        return json.loads(r.text)['album']
    except:
        return None


def artistgetcorrection(name=None):
    ytbot.util.debugmsg('last.fm API call to artist.getCorrection(name={})'
                            .format(name))
    r = requests.post(LASTFM_URL, 
                params = { 'api_key': ytbot.auth.LastFMAuth.apikey(),
                           'method': 'artist.getCorrection',
                           'artist': name,
                           'format': 'json' })
    try:
        return json.loads(r.text)['corrections']['correction']['artist']
    except:
        return None


def albumgetcorrection(name=None, artist=None):
    ytbot.util.debugmsg('last.fm API call to album.getCorrection(name={}, artist={})'
                            .format(name, artist))
    r = requests.post(LASTFM_URL,
                params = { 'api_key': ytbot.auth.LastFMAuth.apikey(),
                           'method': 'album.getCorrection',
                           'artist': artist,
                           'album': name,
                           'format': 'json' })
    try:
        return json.loads(r.text)['corrections']['correction']['album']
    except:
        return None


def trackgetcorrection(name=None, artist=None):
    ytbot.util.debugmsg('last.fm API call to track.getCorrection(name={}, artist={})'
                            .format(name, artist))
    r = requests.post(LASTFM_URL, 
                params = { 'api_key': ytbot.auth.LastFMAuth.apikey(),
                           'method': 'track.getCorrection',
                           'track': name,
                           'artist': artist,
                           'format': 'json' })
    try:
        return json.loads(r.text)['corrections']['correction']['track']
    except:
        return None


class Duration:
    def __init__(self, milliseconds: str):
        ytbot.util.debugmsg('initializing Duration object')
        self._seconds = (int(milliseconds) / 1000)

    def __str__(self):
        return self.minsecs()

    def seconds(self):
        return self._seconds

    def minsecs(self):
        mins, secs = divmod(int(self._seconds), 60)
        return "{}:{}".format(mins, secs)

"""
Artist:
init: provide a value for 'name' if you want to get metadata, otherwise
everything will get filled with defaults for an unknown artist.

update(): force an API request to update the properties of the artist,
even if one has already been made.

get(key): get the artist's property by key. Valid keys for an artist are:
          name, picurl, description, ontour, similar, url
    
    name: str = the artist's name. contains 'Unknown' if artist is unknown.
    picurl: str = URL for artist photo. contains a URL to a generic
        placeholder image if the artist is unknown.
    description: str = artist's full description. contains 'No description 
        available for this artist.' if the artist has no description or
        the artist is unknown.
    ontour: bool = true if the artist is on tour. false if the artist is
        unknown.
    similar: [Artist] = a list of artists related to this artist. empty list
        if artist is unknown.
    url: str = URL for the artist's last.fm profile. empty string if artist
        is unknown.

One low-cost API call will be made at object initialization. Its purpose is
to autocorrect the artist name.
"""
class Artist:
    def __init__(self, name: str = None,
                 picurl = None,
                 ontour = None,
                 similar = None,
                 description = None,
                 url = None,
                 runinitquery = True):
        ytbot.util.debugmsg('initializing Artist object')
        self._d = {}
        self._d['name'] = name
        self._d['url'] = url
        if (name is not None) and (name != 'Unknown') and runinitquery:
            q = artistgetcorrection(name)
            if q is not None:
                self._d['name'] = q['name']
                self._d['url'] = q['url']
        self._d['picurl'] = picurl
        self._d['description'] = description
        self._d['ontour'] = ontour
        self._d['similar'] = similar
        self._retrlock = threading.Lock()
    
    def _settounknown(self):
        self._d['name'] = 'Unknown'
        self._d['picurl'] = 'https://i.imgur.com/VbqM165.png'
        self._d['ontour'] = False
        self._d['description'] = 'No description available for this artist.'
        self._d['similar'] = []
        self._d['url'] = ''
    
    def _retrieve(self):
        # do NOT let go of the lock. we should only ever do
        # a second query if it is explicitly requested using
        # a call to update()
        if self._retrlock.acquire(blocking=False):
            if (self._d['name'] is None) or (self._d['name'] == 'Unknown'):
                self._settounknown()
            else:
                data = artistinfo(name = self._d['name'])
                
                if data is not None:
                    self._d['name'] = data['name']
                    self._d['url'] = data['url']
                    self._d['ontour'] = True if data['ontour'] == '1' else False
                    
                    self._d['similar'] = []
                    if 'similar' in data:
                        for a in data['similar']['artist']:
                            newa = Artist(name = a['name'], 
                                          url = a['url'], 
                                          runinitquery = False)
                            if 'image' in a:
                                newa._d['picurl'] = a['image'][-1]['#text']
                            else:
                                newa._d['picurl'] = 'https://i.imgur.com/VbqM165.png'
                    
                    try:
                        self._d['picurl'] = data['image'][-1]['#text']
                    except KeyError:
                        self._d['picurl'] = 'https://i.imgur.com/VbqM165.png'
                    
                    try:
                        self._d['description'] = data['bio']['content']
                    except KeyError:
                        self._d['description'] = 'No description available for this artist.'
                else:
                    self._settounknown()
    
    def update(self):
        try:
            self._retrlock.release()
        except RuntimeError:
            pass
        self._retrieve()
    
    def get(self, key: str):
        if key in self._d:
            if self._d[key] is None:
                self._retrieve()
                assert self._d[key] is not None
            return self._d[key]
        else:
            raise KeyError
    
    def __str__(self):
        return str(self._d)
    
    def __repr__(self):
        return str(self._d)

"""
Album:
init: provide values for 'name' and 'artist' if you want to get metadata,
otherwise everything will get filled with defaults for an unknown album.

update(): force an API request to update the properties of the album,
even if one has already been made.

get(key): get the album's property. Valid keys for an album are:
        name, artist, picurl, tracklist, description, url
    
    name: str = the album title. contains 'Unknown' if the album is unknown
    artist: Artist = the album artist represented as an Album object. If you
        provide this field at initialization, you may give a string with the
        artist's name instead of an Artist object.
    picurl: str = URL for album artwork.
    tracklist: [Song] = list of Song objects representing the album's track list.
    description: str = description of album. Contains 'No description available 
        for this album.' if no description is available.
    url: str = URL for the album's last.fm profile. empty string if album is
        unknown.

One low-cost API call will be made at object initialization. Its purpose is
to autocorrect the album title and artist name.
"""
class Album:
    def __init__(self, name: str = None,
                 artist = None,
                 picurl = None,
                 tracklist = None,
                 description = None,
                 url = None,
                 runinitquery = True):
        ytbot.util.debugmsg('initializing Album object')
        self._d = {}
        self._d['name'] = name
        self._d['url'] = url
        if (artist is not None) and isinstance(artist, str):
            self._d['artist'] = Artist(name = artist)
            if name is not None and runinitquery:
                q = albumgetcorrection(name=name, 
                                              artist=self._d['artist'].get('name'))
                try: #sometimes last.fm doesn't return a name with this query
                    self._d['name'] = q['name']
                    self._d['url'] = q['url']
                except:
                    self._d['name'] = 'Unknown'
        else:
            self._d['artist'] = Artist(name = 'Unknown')
        self._d['picurl'] = picurl
        self._d['description'] = description
        self._d['tracklist'] = tracklist
        self._retrlock = threading.Lock()
    
    def _settounknown(self):
        self._d['name'] = 'Unknown'
        self._d['picurl'] = 'https://i.imgur.com/VbqM165.png'
        self._d['artist'] = Artist(name = 'Unknown')
        self._d['description'] = 'No description available for this album.'
        self._d['tracklist'] = []
        self._d['url'] = ''
    
    def _retrieve(self):
        # do NOT let go of the lock. we should only ever do
        # a second query if it is explicitly requested using
        # a call to update()
        if self._retrlock.acquire(blocking=False):
            if ((self._d['name'] is None) or (self._d['name'] == 'Unknown')
                or (self._d['artist'].get('name') == 'Unknown')):
                self._settounknown()
            else:
                data = albuminfo(name = self._d['name'], 
                                 artist = self._d['artist'].get('name'))
                
                if data is not None:
                    self._d['name'] = data['name']
                    self._d['url'] = data['url']
                    self._d['tracklist'] = list()
                    for s in data['tracks']['track']:
                        self._d['tracklist'].append(Song(name = s['name'],
                                                         artist = Artist(
                                                             name = s['artist']['name'],
                                                             runinitquery = False),
                                                         url = s['url']))
                    try:
                        self._d['picurl'] = data['image'][-1]['#text']
                    except KeyError:
                        self._d['picurl'] = 'https://i.imgur.com/VbqM165.png'
                    
                    try:
                        self._d['description'] = data['bio']['content']
                    except KeyError:
                        self._d['description'] = 'No description available for this album.'
                else:
                    self._settounknown()
    
    def update(self):
        try:
            self._retrlock.release()
        except RuntimeError:
            pass
        self._retrieve()
    
    def get(self,key: str):
        if key in self._d:
            if self._d[key] is None:
                self._retrieve()
                assert self._d[key] is not None
            return self._d[key]
        else:
            raise KeyError
    
    def __str__(self):
        return str(self._d)
    
    def __repr__(self):
        return str(self._d)

"""
Song:
init: provide values for 'name' and 'artist' as strings if you want to get
metadata. otherwise, everything will be filled with defaults for an unknown
song.

update(): force an API request to update the properties of the album, even
if one has already been made.

get(key): get the song's property, Valid keys for a song are:
    
    name: str = the song title. contains 'Unknown' if the song is unknown.
    duration: Duration = the song's duration, per metadata. does not
        necessarily match the length of the streaming service's artifact.
    artist: Artist = the song's artist. Refer to the Artist class for
        properties of an unknown artist.
    album: Album = the song's album. Refer to the Album class for
        properties of an unknown album.
    description: str = description of song, if available. If not
        avalable, contains 'No description available for this song.'
    url: str = last.fm URL for the song. if a YouTube URL was provided,
        and the last.fm request fails, this will contain the youtube URL.
        otherwise, will contain empty string.
One low-cost API call will be made at object initialization. Its purpose is
to autocorrect the song title and artist name.
"""
class Song:
    def __init__(self, name: str = None,
                 duration: Duration = None,
                 artist: Artist = None,
                 album: Album = None,
                 description = None,
                 url: str = None,
                 runinitquery = True,
                 yturl: str = ''):
        ytbot.util.debugmsg('initializing Song object')
        self._d = {}
        self._d['yturl'] = yturl
        if ((artist is not None) 
            and isinstance(artist, str) 
            and (name is not None) 
            and (name != 'Unknown')
            and runinitquery):
            q = trackgetcorrection(name = name,
                                   artist = artist)
            try:
                self._d['name'] = q['name']
                self._d['url'] = q['url']
                self._d['artist'] = Artist(name = q['artist']['name'],
                                           runinitquery = False)
            except:
                self._d['name'] = 'Unknown'
                self._d['url'] = ''
                self._d['artist'] = Artist(name = 'Unknown',
                                           runinitquery = False)
        else:
            self._d['artist'] = Artist(name = 'Unknown',
                                           runinitquery = False)
            self._d['name'] = 'Unknown'
            self._d['url'] = ''
        if album is not None and isinstance(album, str):
                self._d['album'] = Album(name = album,
                                         runinitquery = False)
        else:
            self._d['album'] = album
        if (duration is not None) and isinstance(duration, Duration):
            self._d['duration'] = duration
        elif (duration is not None) and isinstance(duration, int):
            self._d['duration'] = Duration(duration)
        else:
            self._d['duration'] = None
        self._d['description'] = description
        self._retrlock = threading.Lock()
    
    def _settounknown(self):
        #self._d['name'] = 'Unknown'
        self._d['description'] = 'No description available for this track.'
        self._d['url'] = self._d['yturl']
        self._d['duration'] = Duration(0)
        self._d['artist'] = Artist(name = 'Unknown',
                                   runinitquery = False)
        self._d['album'] = Album(name = 'Unknown',
                                 artist = Artist(name = 'Unknown',
                                                 runinitquery = False),
                                 runinitquery = False)
    
    def _retrieve(self):
        # do NOT let go of the lock. we should only ever do
        # a second query if it is explicitly requested using
        # a call to update()
        if self._retrlock.acquire(blocking=False):
            if ((self._d['name'] is None) 
                    or (self._d['name'] == 'Unknown')
                    or (self._d['artist'] is None) 
                    or (self._d['artist'].get('name') == 'Unknown')):
                self._settounknown()
            
            else:
                data = trackinfo(name = self._d['name'], 
                                 artist = self._d['artist'].get('name'))
                
                if data is not None:
                    self._d['name'] = data['name']
                    self._d['url'] = data['url']
                    self._d['duration'] = Duration(int(data['duration']))
                    if 'album' in data:
                        self._d['album'] = Album(name = data['album']['title'],
                                                 artist = data['album']['artist'],
                                                 url = data['album']['url'],
                                                 runinitquery = False)
                        if 'image' in data['album']:
                            self._d['album']._d['picurl'] = data['album']['image'][-1]['#text']
                    else:
                        self._d['album'] = Album(name = 'Unknown',
                                                 artist = Artist(name = 'Unknown',
                                                                 runinitquery = False),
                                                 runinitquery = False)
                    
                    if 'artist' in data:
                        self._d['artist'] = Artist(name = data['artist']['name'],
                                                   url = data['artist']['url'],
                                                   runinitquery = False)
                    else:
                        self._d['artist'] = Artist(name = 'Unknown', runinitquery = False)
                    
                    if 'wiki' in data:
                        self._d['description'] = data['wiki']['content']
                    else:
                        self._d['description'] = 'No description available for this track.'
                else:
                    self._settounknown()
    
    def update(self):
        try:
            self._retrlock.release()
        except RuntimeError:
            pass
        self._retrieve()
    
    def get(self,key: str):
        if key in self._d:
            if self._d[key] is None:
                self._retrieve()
                assert self._d[key] is not None
            return self._d[key]
        else:
            raise KeyError
    
    def __str__(self):
        return str(self._d)
    
    def __repr__(self):
        return str(self._d)
