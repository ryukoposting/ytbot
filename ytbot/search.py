# match youtube video data to track metadata

import ytbot
import re
from ytbot import damlev
from ytbot import util
from ytbot import lastfm

def getsong(ytid: str):
    yt = None
    try:
        yt = ytbot.youtube.listvideo(ytbot.auth.GoogleAuth.session(),
            id = ytid,
            part = 'snippet')
    except: pass
    
    if (yt is None) or len(yt['items']) == 0:
        return ytbot.lastfm.Song(name='Unknown', artist='Unknown')
    
    else:
        video = yt['items'][0]
        #channel = ytbot.youtube.listchannel(ytbot.auth.GoogleAuth.session(),
                                            #part='snippet',
                                            #id = video['snippet']['channelId'])
        title = video['snippet']['title']
        channel = video['snippet']['channelTitle']
        
        ytbot.util.debugmsg('TITLE={}'.format(title))
        ytbot.util.debugmsg('CHANNEL={}'.format(channel))
        
        # VEVO almost always formats like this
        if 'VEVO' in channel:
            title = re.sub(r'[(\[].*emaster.*[\])]', '', title)
            title = re.sub(r'[(\[].*fficial.*[\])]', '', title)
            title = re.sub(r'[(\[].*ideo.*[\])]', '', title)
            
            if ' - ' in title:
                spl = title.split(' - ')
                
                track = lastfm.Song(name=spl[0], artist=spl[1])
                track.update()
                return track
        
        # channel name is probably the artist if it shows up exactly in the video title
        elif channel in title:
            title = re.sub(r'[(\[].*emaster.*[\])]', '', title)
            title = re.sub(r'[(\[].*fficial.*[\])]', '', title)
            title = re.sub(r'[(\[].*ideo.*[\])]', '', title)
            title = re.sub(r'[(\[].*emix.*[\])]', '', title)
            title = re.sub(r'[(\[].*[\])]', '', title)
                
            #TODO: fix this garbage
            songname = title.replace(channel, '').replace('-', '').replace(':', '').replace('"','').strip()
            
            track = lastfm.Song(name=songname,
                                artist=channel,
                                yturl='https://www.youtube.com/watch?v={}'.format(ytid))
            track.update()
            return track
        
        # last-ditch effort, just split and pray
        else:
            title = re.sub(r'[(\[].*emaster.*[\])]', '', title)
            title = re.sub(r'[(\[].*fficial.*[\])]', '', title)
            title = re.sub(r'[(\[].*ideo.*[\])]', '', title)
            title = re.sub(r'[(\[].*emix.*[\])]', '', title)
            title = re.sub(r'[(\[].*[\])]', '', title)
            channel = re.sub(r'official', '', channel)
            channel = re.sub(r' - Topic', '', channel)
            title = title.replace('Tha ', 'The ')
            
            channel = channel.strip()
            title = title.strip()
            
            ytbot.util.debugmsg('last-ditch attempt will search for channel="{}" title="{}"'.format(channel,title))
            
            if ' - ' in title:
                spl = title.split(' - ')
                track = lastfm.Song(name=spl[0],
                                    artist=spl[1],
                                    yturl='https://www.youtube.com/watch?v={}'.format(ytid))
                
                if track.get('name') == 'Unknown':
                    track = lastfm.Song(name=spl[1],
                                    artist=spl[0],
                                    yturl='https://www.youtube.com/watch?v={}'.format(ytid))
                
                return track
            
            else:
                #TODO: fix this garbage
                title = re.sub(r'[(\[].*emaster.*[\])]', '', title)
                title = re.sub(r'[(\[].*fficial.*[\])]', '', title)
                title = re.sub(r'[(\[].*ideo.*[\])]', '', title)
                title = re.sub(r'[(\[].*emix.*[\])]', '', title)
                title = re.sub(r'[(\[].*[\])]', '', title)
            	
                track = lastfm.Song(name=title,
                                    artist=channel,
                                    yturl='https://www.youtube.com/watch?v={}'.format(ytid))
                return track
