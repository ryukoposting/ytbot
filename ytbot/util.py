import ytbot
import re
import inspect

# NOTE: this will consume the preceding character, in addition to the quote!
SPLIT_QUOT = re.compile(r'[^\\]"')
PRETTIFY_INDENT_DEPTH = 3

def debugmsg(msg):
    if ytbot.SHOW_DEBUG:
        msg = str(msg)
        print('ytbot: {}'.format(msg))
        if ytbot.VERBOSE_DEBUG:
            curframe = inspect.currentframe()
            caller = inspect.getouterframes(curframe, 2)
            print('  Call stack:')
            for i in range(1, len(caller)):
                fstr = '    {}({}: {})'.format(
                        caller[i].filename, caller[i].function, caller[i].lineno)
                print(fstr)


# TODO: less hideous version of this?
# most of the complexity is just dealing with the two different string formats
def prettifydict(d):
    d = str(d)
    lvl = 0
    # high while inside a string.
    skip = False
    ignore = False
    # true on the first pass after skip is set high.
    firstinskip = False
    # this string starts with '" or "', signifying that the first of the two
    # chars should be ignored until a non-escaped version of the second is found.
    longskip = False
    # when inside a string, esc_quot holds the quote character that is innately
    # escaped by the string, and nonesc_quot holds the quote character that must
    # be preceded by a \ in order for it to be escaped
    esc_quot = None
    nonesc_quot = None
    
    out = ''
    for ch in d:
        if (ch == "'" or ch == '"') and not skip:
            firstinskip = True
            skip = True
            esc_quot = '"' if ch == "'" else "'"
            nonesc_quot = ch
        
        elif not skip:
            ignore = False
            if (ch == '{') or (ch == '['):
                lvl += 1
                ch = ch + '\n' + (' ' * lvl * PRETTIFY_INDENT_DEPTH)
            elif (ch == '}') or (ch == ']'):
                lvl -= 1 if lvl > 0 else 0
                ch = '\n' + (' ' * lvl * PRETTIFY_INDENT_DEPTH) + ch
            elif ch == ',': ch = ',\n' + (' ' * lvl * PRETTIFY_INDENT_DEPTH)
            elif ch == ' ': ch = ''
            elif ch == ':': ch = ': '
        
        else:
            if firstinskip:
                if ch == nonesc_quot:   # only true if parsing empty string
                    skip = False
                else:
                    longskip = (ch == esc_quot)
                firstinskip = False
            else:
                if not ignore:
                    if ch == '\\':
                        ignore = True
                    elif ch == esc_quot and longskip:
                        longskip = False
                    elif ch == nonesc_quot and not longskip:
                        skip = False
                else:
                    ignore = False
        
        out = out + ch
    return out


# "Replace Not In Quotes"
def _rniq(s, a, b):
    return _rniq_replace('', s, a, b)
        

def _rniq_replace(cur, rem, a, b):
    spl = SPLIT_QUOT.split(rem, maxsplit = 1)
    if len(spl) == 2:
        consumed = rem[len(spl[0])]         # see note for SPLIT_QUOT
        return _rniq_skip(cur + spl[0].replace(a, b) + consumed + '"', spl[1], a, b)
    else:
        return cur + rem.replace(a, b)


def _rniq_skip(cur, rem, a, b):
    spl = SPLIT_QUOT.split(rem, maxsplit = 1)
    if len(spl) == 2:
        consumed = rem[len(spl[0])]         # see note for SPLIT_QUOT
        return _rniq_replace(cur + spl[0] + consumed + '"', spl[1], a, b)
    else:
        return cur + rem
