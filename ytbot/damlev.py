# damerau-levenshtein distance

from difflib import SequenceMatcher

VERY_BIG = 0xFFFFFFFF

def strdist(a: str, b: str):
    return SequenceMatcher(None, a, b).ratio()
    #return strdist_calc(a, b, len(a), len(b))

#def strdist_calc(a: str, b: str, i: int, j: int):
    #return 0 if (i == 0) and (j == 0) else min(
            #VERY_BIG if i <= 0 else strdist_calc(a, b, i - 1, j) + 1,
            #VERY_BIG if j <= 0 else strdist_calc(a, b, i, j - 1) + 1,
            #VERY_BIG if (i <= 0) or (j <= 0) else strdist_calc(a, b, i - 1, j - 1) + 
                            #(1 if a[i] == b[j] else 0)
            #VERY_BIG if ((i <= 1) or (j <= 1)) or (a[i] != b[j - 1]) or (a[i - 1] != b[j]) else strdist_calc(a, b, i - 2, j - 2)
        #)
